import React from "react";
import './components.css';
import { translate } from '../services/translations';
const text = translate('i-want-to-voluteer');

const WantToVolunteerBtn = () => { 
    return ( 
        <button className="s-btn voluteer" role="button">
        { text }
    </button>
); }

export default WantToVolunteerBtn;