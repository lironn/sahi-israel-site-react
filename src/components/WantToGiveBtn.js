import React from "react";
import './components.css';
import { translate } from '../services/translations';
const text = translate('i-want-to-donate');

const WantToGiveBtn = () => { 
    return ( 
            <button className="s-btn donate" role="button">
                { text }
            </button>
); }

export default WantToGiveBtn;