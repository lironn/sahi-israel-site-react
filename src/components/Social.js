import React from "react";
import '../css/app.css';

const Social = () => { 
    return ( 
            <div className="social">
                <img tabIndex="9" src={require('../img/igul2.png')} alt="igol logo link to home page" className="n-logo"></img>
                <img tabIndex="9" src={require('../img/midot.png')} alt="midot logo link to home page" className="n-logo"></img>
                <img tabIndex="9" src={require('../img/logos/nochach.png')} alt="sachi logo link to home page" className="n-logo"></img>
                <a tabIndex="10" target="_blank" href="https://www.facebook.com/%D7%A1%D7%97%D7%99-%D7%A1%D7%99%D7%99%D7%A8%D7%AA-%D7%97%D7%A1%D7%93-%D7%99%D7%97%D7%95%D7%93%D7%99%D7%AA-275095383548/?fref=ts">
                    <img src={require('../img/fb-circ.png')} alt="דף פייסבוק" className="fb"/>
                </a>
                <a tabIndex="11" target="_blank" href="https://www.youtube.com/user/SahiIsrael">
                    <img src={require("../img/youtube_circ.png")} alt="ערוץ יוטיוב" className="yt"/>
                </a>
                <a tabIndex="12" target="_blank" href="http://www.mashmaut.org.il">
                    <img src={require("../img/atzmaut.png")} alt="עצמאות עם משמעות" className="yt"/>
                </a>			
            </div>
); }

export default Social;