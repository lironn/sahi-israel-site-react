import React from "react";
import './components.css';
import { translate } from '../services/translations';
import WantToGiveBtn from './WantToGiveBtn';
import WantToVolunteerBtn from './WantToVolunteerBtn';
const text = translate('i-want-to-voluteer');

const StartGivingCube = () => { 
    return ( 
        <div className="start-give-wrapper cube">
         { text }
            <div className="start-give-actions">
                <WantToGiveBtn/>
                <WantToVolunteerBtn/>
            </div>
        </div>
); }

export default StartGivingCube;