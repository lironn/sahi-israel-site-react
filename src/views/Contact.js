import React, { Component } from 'react';
import DonationFooter from './DonationFooter';
import '../css/app.css';
import './views.css';

class Contact extends Component {
  render() {
    return (
      <>
        <section className="contact">
          <aside>
            <div className="h3">
              <h3 tabIndex="14">קשר</h3>
            </div>
            <h3 className="h3">שלחו לנו מייל</h3>
            <div className="form">
              <form name="contactForm" onSubmit={(e) => { console.log('go'); e.preventDefault();}}>
                <div>
                    <div>
                        <label tabIndex="18" for="contactName">שם
                        </label>
                        <input tabIndex="19" name="contactName" id="contactName" type="text" className="formInput" required="" aria-invalid="true" />
                        
                        <label tabIndex="20" for="contactPhone">טלפון
                        </label>
                        <input tabIndex="21" name="contactPhone" required="" aria-invalid="true" />
                      </div>
                      <div>
                      <label tabIndex="24" for="contactEmail">מייל
                        </label>
                        <input tabIndex="25" name="contactEmail" id="contactEmail" type="email" className="formInput" required="" aria-invalid="true" />

                        <label tabIndex="24" for="contactEmail">תוכן
                        </label>
                        <textarea tabIndex="31" name="contactMsg" id="contactMsg" cols="16" rows="6" className="formTextArea" required="" aria-invalid="true"></textarea>

                      </div>
                </div>
                <input className="sendBtn" type="submit" tabIndex="32" value="שלח" />
              </form>
            </div>
            <img src={require('../img/mail_vector_icon_white.svg')} alt="mail" className="mail"></img>
          </aside>
          <article className="info">
          למידע נוסף על עמותת נכח ופעילויות סח"י
            לשיתופי פעולה, הצטרפות ולפתיחת סיירות חדשות–
             השאירו הודעה ב <a href="tel:+972544-634546"> 054-4634546</a>
          </article>
        </section>
        <DonationFooter/>
      </>
    );
  }
}

export default Contact;
