import React from "react";
import './views.css';
import WantToGiveBtn from '../components/WantToGiveBtn';
import WantToVolunteerBtn from '../components/WantToVolunteerBtn';

const DonationFooter = () => { 
    return ( 
        <section>
            <div className="logo-cotainer">
            <img src={require('../img/logo.PNG')} alt="sachi logo link to home page" className="logo"></img>
            </div>
            <section className="actions">
            <WantToGiveBtn></WantToGiveBtn>
            <WantToVolunteerBtn></WantToVolunteerBtn>
            </section>
        </section>
); }

export default DonationFooter;