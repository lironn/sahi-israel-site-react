import React, { Component } from 'react';
import DonationFooter from './DonationFooter';

class HowCanYouHelp extends Component {
  render() {
    return (
      <>
        <section className="activity">
        <aside>
        <div className="h3">
            <h3>נתינה</h3>
        </div>
        <div>
            <span tabIndex="12" className="clickable" role="button">
                <p className="selected">
                    נכח – נתינה כדרך חיים
                    </p>
                <p></p>
                {/* <img src="img/hr.PNG" className="myHr" alt=""> */}
            </span><span tabIndex="13" className="clickable" role="button">
                <p className="ng-binding">
                    התנדבות בסיירות
                    </p>
                <p></p>
                {/* <img src="img/hr.PNG" className="myHr" alt=""> */}
            </span>
        </div>
        <div className="frame">
            <iframe tabIndex="15" frameborder="0" title="0" byline="0" portrait="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" src="https://player.vimeo.com/video/164921245?autoplay=0">
            </iframe>
        </div>
    </aside>
        </section>
        <DonationFooter/>
      </>
    );
  }
}

export default HowCanYouHelp;
