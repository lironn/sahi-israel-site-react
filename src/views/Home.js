import React, { Component } from 'react';
import StartGivingCube from '../components/StartGivingCube';
import { data } from '../static-data/data';
import { translate } from '../services/translations';

class Home extends Component {
  render() {
    return (
      <>
        <section className="frames">
          <img src={require('../img/cover.jpg')} alt="sachi cover home page" className="cover"></img>
        </section>
        <section className="grid">
        <div className="start-give-wrapper cube blue">
          <span> <h5> כל מה שצריך לדעת על סח״י ב3 דק </h5> </span>
          <span> <h4> לגלריית הוידאו </h4> </span>

        <iframe width="360" height="315" src="https://www.youtube.com/embed/nxkgUqLM8cc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
        </iframe>
        </div>
        <div className="start-give-wrapper cube blue">
        <span className="long-text"> <h5> {translate('sahi-home-explanation')} </h5> </span>
        </div>
        <div className="start-give-wrapper cube">
        </div>
        <div className="start-give-wrapper cube">
        </div>
        </section>
      </>
    );
  }
}

export default Home;
