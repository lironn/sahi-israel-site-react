import React, { Component } from 'react';

class People extends Component {
  render() {
    return (
      <>
        <section className="activity">
        <div autoscroll="true">
        <div className="people">
    
    <aside>
        <div className="h3">
            <h3 tabIndex="14">אנשים</h3>
        </div>
        <div className="activity-nav">
            <span>
                <a href="#people">
                    <p tabIndex="15" className="regular selected">
                        האנשים שלנו
                     </p>
                </a>
                {/* <img ng-if="!isMobile" src="img/hr.PNG" className="myHr" alt=""> */}
            </span>
            <span>
                <a href="#partners">
                    <p tabIndex="16">
                        השותפים שלנו
                    </p>
                </a>
                {/* <img ng-if="!isMobile" src="img/hr.PNG" class="myHr ng-scope" alt=""> */}
            </span>
            <span>
                <a href="#thanks">
                    <p tabIndex="16">
                        תודות
                    </p>
                </a>
             {/* <img ng-if="!isMobile" src="img/hr.PNG" class="myHr" alt=""> */}
            </span>
         </div>
    </aside>

    {/* <article ng-if="!isMobile">
        <h1 tabIndex="17" className="light">האנשים שלנו</h1>
    </article> */}

</div>

<div className="peopleDesc">
    <div className="boxImg">
        {/* <img tabIndex="18" ng-src="img/people/oded.jpg" alt="image of sachi manager" class="interviewImg" src="img/people/oded.jpg"> */}
    </div>
    <span class="separator"> </span>
    <article>
        <span className="peopleTitle">
            <p tabIndex="19" editable-textarea="section.titlePrimary" e-rows="7" e-cols="40" e-form="textBtnForm1" onaftersave="vm.updateModel()" class="strong weight primaryTitle editable">
                עודד וייס
            </p>
            <p tabIndex="19" e-rows="1" e-cols="40" e-form="textBtnForm2" onaftersave="vm.updateModel()" editable-textarea="section.titleSecondary" class="strong light mb-20 mt-20 editable">
                נשיא מייסד
            </p>
        </span>

        <p tabIndex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph ligh editable" ng-bind-html="section.textPrimaryPeople | nl2br">איש חינוך למעלה מ-20 שנה, הקים ב־2007 את עמותת נכח - נתינה כדרך חיים (ע"ר) והגה את תכניות העמותה- סח"י, חש"ן, שה"ם ועוד מתוך תפיסה ש"הנתינה מחברת אותנו למי שאנחנו באמת", כיחיד וכחברה. משמש כיום כמנהל החינוכי של העמותה</p>

        <p tabIndex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light editable">
            
        </p>
    </article>
</div><div ng-repeat="section in vm.data.sections" className="peopleDesc">
    <div className="boxImg">
        {/* <img tabIndex="18" ng-src="img/people/avraham.jpg" alt="image of sachi manager" class="interviewImg" src="img/people/avraham.jpg"> */}
    </div>
    <span className="separator"> </span>
    <article>
        <span className="peopleTitle">
            <p tabIndex="19" editable-textarea="section.titlePrimary" e-rows="7" e-cols="40" e-form="textBtnForm1" onaftersave="vm.updateModel()" class="strong weight primaryTitle ng-scope ng-binding editable">
                אברהם חיון
            </p>

            <p tabIndex="19" e-rows="1" e-cols="40" e-form="textBtnForm2" onaftersave="vm.updateModel()" editable-textarea="section.titleSecondary" class="strong light mb-20 mt-20 ng-scope ng-binding editable">
                מנכ"ל ושותף מייסד
            </p>
        </span>

        <p tabIndex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-binding ng-scope editable" ng-bind-html="section.textPrimaryPeople | nl2br">יזם חברתי, הקים יחד עם עודד את תכנית סח"י ב-2009. כמנכ"ל, אברהם פועל ליצירת שיתופי פעולה עם תורמים ושותפים רבים, ונשאר כל הזמן בקשר עם השטח ועם בני הנוער.</p>

        <p tabIndex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-scope ng-binding editable">
            "אני מודה על הזכות שיש לי לעשות עשייה כ"כ משמעותית, בעיניי העשייה שלנו היא הדרך לחולל שינוי חברתי אמיתי. כשאני פוגש את הנוער והצעירים בשטח - אני יודע שהם הולכים להיות המנהיגים הבאים של החסד הזה".
        </p>

    </article>
</div>
<div className="peopleDesc">
    <div className="boxImg">
        {/* <img tabIndex="18" ng-src="img/people/ronen.jpg" alt="image of sachi manager" class="interviewImg" src="img/people/ronen.jpg"> */}
    </div>
    <span className="separator"> </span>
    <article>
        <span className="peopleTitle">
            <p tabIndex="19" editable-textarea="section.titlePrimary" e-rows="7" e-cols="40" e-form="textBtnForm1" onaftersave="vm.updateModel()" class="strong weight primaryTitle ng-scope ng-binding editable">
                רונן כהן
            </p>

            <p tabIndex="19" e-rows="1" e-cols="40" e-form="textBtnForm2" onaftersave="vm.updateModel()" editable-textarea="section.titleSecondary" class="strong light mb-20 mt-20 ng-scope ng-binding editable">
                רכז תוכן והדרכה
            </p>
        </span>

        <p tabIndex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-binding ng-scope editable" ng-bind-html="section.textPrimaryPeople | nl2br">עובד עם בני נוער בסיכון ב-14 השנים האחרונות בפרוייקטים פורמאליים ובלתי פורמאליים, בעל תואר שני בפילוסופיה של החינוך. במסגרת תפקידו כרכז תוכן והדרכה הוא מלווה את הרכזים, כותב תכנים, מעביר הכשרות פרטניות וקבוצתיות לצ'יפים ואחראי על ניהול ידע, תיעוד ומחקר. </p>


        <p tabIndex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-scope ng-binding editable">
            "עבורי, הבאה לכדי קיום של קהילות נתינה הינה המטרה הנעלה ביותר שיש לשאוף אליה, ואני אסיר תודה על כך שאני זוכה לקחת חלק בארגון שמטרתו קידום חזון זה".
        </p>

    </article>
</div><div className="peopleDesc">
    <div className="boxImg">
        {/* <img tabIndex="18" ng-src="img/people/inbar.jpg" alt="image of sachi manager" class="interviewImg" src="img/people/inbar.jpg"> */}
    </div>
    <span className="separator"> </span>
    <article>
        <span className="peopleTitle">
            <p tabIndex="19" editable-textarea="section.titlePrimary" e-rows="7" e-cols="40" e-form="textBtnForm1" onaftersave="vm.updateModel()" class="strong weight primaryTitle ng-scope ng-binding editable">
                ענבר רז רוטשילד
            </p>

            <p tabIndex="19" e-rows="1" e-cols="40" e-form="textBtnForm2" onaftersave="vm.updateModel()" editable-textarea="section.titleSecondary" class="strong light mb-20 mt-20 ng-scope ng-binding editable">
                רכזת בוגרים
            </p>
        </span>

        <p tabIndex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-binding ng-scope editable" ng-bind-html="section.textPrimaryPeople | nl2br">בעלת תואר בצילום מהאקדמיה לאמנות בצלאל ותעודת הוראה. מצ'יפית בירושלים, הפכה ענבר לרכזת תחום הבוגרים הארצית של הארגון.  כיום היא בונה את התוכנית עבור הצעירים בוגרי סח"י ומלווה בוגרים פרטנית במענים שונים כמו צבא, תעסוקה ודיור.</p>

        <p tabIndex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-scope ng-binding editable">
            "הנערים והנערות בסח"י הפכו להיות אחים ואחיות קטנים עבורי, ויתרה מזאת לימדו אותי רבות על עצמי, על התרבות שאני גדלתי עליה ועל הדברים שאני מעוניינת לשנות בעצמי ובסביבה שלי."
        </p>

    </article>
</div>
<div className="peopleDesc">
    <div className="boxImg">
        {/* <img tabIndex="18" ng-src="img/people/valeria.jpg" alt="image of sachi manager" class="interviewImg" src="img/people/valeria.jpg"> */}
    </div>
    <span className="separator"> </span>
    <article>
        <span class="peopleTitle">
            <p tabIndex="19" editable-textarea="section.titlePrimary" e-rows="7" e-cols="40" e-form="textBtnForm1" onaftersave="vm.updateModel()" class="strong weight primaryTitle ng-scope ng-binding editable">
                ולריה פוליאקוב
            </p>

            <p tabIndex="19" e-rows="1" e-cols="40" e-form="textBtnForm2" onaftersave="vm.updateModel()" editable-textarea="section.titleSecondary" class="strong light mb-20 mt-20 ng-scope ng-binding editable">
                רכזת מטה 
            </p>
        </span>

        <p tabIndex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-binding ng-scope editable" ng-bind-html="section.textPrimaryPeople | nl2br">בוגרת תואר שני בניהול מלכ"רים ומגדר. כרכזת מטה היא אחראית על הפן האדמינסטרטיבי. בנוסף, היא עובדת על פיתוח תכנית GPS -   Girl Power Sahi, שמטרתה העצמת נערות בסיכון. </p>


        <p tabIndex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-scope ng-binding editable">
            "להיות בפעילות ולראות את ההשפעה של הנתינה על בני הנוער מרגש אותי כל פעם מחדש. הצוות של סח"י עובד מהלב וזה מה שיוצר את האווירה הכל כך מיוחדת בארגון."
        </p>

    </article>
</div><div ng-repeat="section in vm.data.sections" class="peopleDesc ng-scope">
    <div class="boxImg">
        {/* <img tabIndex="18" ng-src="img/people/ori.jpg" alt="image of sachi manager" class="interviewImg" src="img/people/ori.jpg"> */}
    </div>
    <span class="separator"> </span>
    <article>
        <span class="peopleTitle">
            <p tabIndex="19" editable-textarea="section.titlePrimary" e-rows="7" e-cols="40" e-form="textBtnForm1" onaftersave="vm.updateModel()" class="strong weight primaryTitle ng-scope ng-binding editable">
                אורי צדוק
            </p>

            <p tabIndex="19" e-rows="1" e-cols="40" e-form="textBtnForm2" onaftersave="vm.updateModel()" editable-textarea="section.titleSecondary" class="strong light mb-20 mt-20 ng-scope ng-binding editable">
                רכז ירושלים והסביבה
            </p>
        </span>

        <p tabIndex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-binding ng-scope editable" ng-bind-html="section.textPrimaryPeople | nl2br">הצ'יף הראשון של סח"י בירושלים. נשוי עם שני ילדים, ירושלמי. היום הוא רכז אזור ירושלים, שותף בהדרכות של כל הצ'יפים, בליווי הלוגיסטיקי והרוחני. חי את סח"י בכל נימיו ונשמתו, ילדיו ואשתו מגבים את העשייה האין סופית שלו בעמותה.</p>

        <p tabIndex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-scope ng-binding editable">
            "סח"י בשבילי היא הדבר הכי גדול בעולם". 
        </p>

    </article>
</div><div className="peopleDesc">
    <div class="boxImg">
        {/* <img tabIndex="18" ng-src="img/people/ohad.jpg" alt="image of sachi manager" class="interviewImg" src="img/people/ohad.jpg"> */}
    </div>
    <span class="separator"> </span>
    <article>
        <span class="peopleTitle">
            <p tabIndex="19" editable-textarea="section.titlePrimary" e-rows="7" e-cols="40" e-form="textBtnForm1" onaftersave="vm.updateModel()" class="strong weight primaryTitle ng-scope ng-binding editable">
                אוהד שעאר
            </p>

            <p tabindex="19" e-rows="1" e-cols="40" e-form="textBtnForm2" onaftersave="vm.updateModel()" editable-textarea="section.titleSecondary" class="strong light mb-20 mt-20 ng-scope ng-binding editable">
                רכז שטח
            </p>
        </span>

        <p tabindex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-binding ng-scope editable" ng-bind-html="section.textPrimaryPeople | nl2br">אהד הוא הצ'יף של קריית מלאכי ומלווה קבוצות נוספות. נשוי ואב לשתיים. אוהב את מה שהוא עושה, אוהב אנשים ועשיית טוב.</p>

        <p tabindex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-scope ng-binding editable">
            "סח"י בשבילי זה סיפוק אדיר, אני מביא הביתה תכלית של נתינה, של חסד ואהבת הזולת. זוהי הזדמנות בשבילי לאפשר לאדם להגיע לטוב שבו, בלי לשפוט או להעביר ביקורת , תמיד לראות את הטוב שבכל אחד".
        </p>
    </article>
</div><div class="peopleDesc ng-scope">
    <div class="boxImg">
        {/* <img tabIndex="18" ng-src="img/people/yochai.jpg" alt="image of sachi manager" class="interviewImg" src="img/people/yochai.jpg"> */}
    </div>
    <span className="separator"> </span>
    <article>
        <span class="peopleTitle">
            <p tabindex="19" editable-textarea="section.titlePrimary" e-rows="7" e-cols="40" e-form="textBtnForm1" onaftersave="vm.updateModel()" class="strong weight primaryTitle ng-scope ng-binding editable">
                יוחאי בוחבוט
            </p>

            <p tabindex="19" e-rows="1" e-cols="40" e-form="textBtnForm2" onaftersave="vm.updateModel()" editable-textarea="section.titleSecondary" class="strong light mb-20 mt-20 ng-scope ng-binding editable">
                רכז אזור דרום
            </p>
        </span>

        <p tabindex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-binding ng-scope editable" ng-bind-html="section.textPrimaryPeople | nl2br">שירת שירות קרבי ארוך ומשמעותי כקצין, השתחרר בתפקיד מ"פ מבצעית ובחר להמשיך את הנתינה באזרחות בעיר הולדתו- קרית גת. יוחאי מרכז את אזור הדרום של סח"י, תפקיד הכולל עבודה שוטפת במתן מענה לקבוצות, ובשיתופי פעולה עם הגופים השונים והרשויות. </p>


        <p tabindex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-scope ng-binding editable">
            "בצבא, מצאתי את המשמעות האמתית בעבודה עם החיילים, זה נתן לי דרייב עצום. חיפשתי את ההגשמה הזו באזרחות ומצאתי את זה דרך סח"י".
        </p>

    </article>
</div><div class="peopleDesc ng-scope">
    <div class="boxImg">
        {/* <img tabindex="18" ng-src="img/people/avner.jpg" alt="image of sachi manager" class="interviewImg" src="img/people/avner.jpg"> */}
    </div>
    <span class="separator"> </span>
    <article>
        <span class="peopleTitle">
            <p tabIndex="19" editable-textarea="section.titlePrimary" e-rows="7" e-cols="40" e-form="textBtnForm1" onaftersave="vm.updateModel()" class="strong weight primaryTitle ng-scope ng-binding editable">
                אבנר נעים
            </p>
            <p tabIndex="19" e-rows="1" e-cols="40" e-form="textBtnForm2" onaftersave="vm.updateModel()" editable-textarea="section.titleSecondary" class="strong light mb-20 mt-20 ng-scope ng-binding editable">
                רכז אזור מרכז
            </p>
        </span>

        <p tabIndex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-binding ng-scope editable" ng-bind-html="section.textPrimaryPeople | nl2br">נשוי עם שני ילדים. השתחרר משירות של 10 שנים בצבא, במהלך שירותו פיקד על חיילים רבים. בעל תואר ראשון בחברה וביטחון. החליט להמשיך את הנתינה למדינה במסגרת סח"י, היום מרכז את אזור המרכז. </p>

        <p tabIndex="19" e-rows="7" e-cols="40" e-form="textBtnForm3" onaftersave="vm.updateModel()" editable-textarea="section.textPrimaryPeople" class="descParagraph light ng-scope ng-binding editable">
            "סח"י בשבילי זאת הזדמנות לתת הזדמנות לאנשים מזדמנים"
        </p>

    </article>
</div>

</div>
        </section>
      </>
    );
  }
}

export default People;
