import React, { Component } from 'react';
import '../css/app.css';
import { translate } from '../services/translations';
import { data } from '../static-data/data';
const { transparency } = data;
const years = Object.keys(transparency.years);

class Transparency extends Component {  
  state = { year: '2014' };

  renderYearsOptions = 
  (years) => years.map(year => (<option key={year}> {year}</option>));
  
  renderYearsHrefs = 
  (year) => year.map((item, i) => 
  (<a key={i} target="_blank" href={'http://sahi-israel.org/' + item.url}> {translate(item.title)} </a>));

  handleChange = (e) => {
    console.log(e.target.value);
    this.setState({
      year:  e.target.value
    });
  }

  render() {
    return (
      <>
        <section className="frames">
        <div className="transparencyHeader">
          <aside>
              <div className="title">
                  <h3 tabIndex="14">{translate('Transparency')}</h3>
              </div>
          </aside>
          <article>
              <h1 tabIndex="15" className="light">{translate('Financial_documents')}</h1>
          </article>
        </div>
        <div className="transparencyContent">
          <div className="dropdown">
              <select onChange={this.handleChange} className="dropbtn">
                  {this.renderYearsOptions(years)}
              </select>
          </div>
          <div className="docList">
            <div className="mb-5">
                {this.renderYearsHrefs(transparency.years[this.state.year])}
            </div>
        </div>
    </div>
        </section>
      </>
    );
  }
}

export default Transparency;
