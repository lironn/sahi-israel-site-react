import React, { Component } from 'react';
import Social  from './components/Social';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Home from "./views/Home";
import Contact from "./views/Contact";
import HowCanYouHelp from "./views/HowCanYouHelp";
import People from "./views/People";
import Transparency from "./views/Transparency";
import './App.css';
import './css/app.css';

class App extends Component {
  render() {
    return (
      <div className="App">
      <Router>
        <div className="app-flex">
        <header className="App-header">
        <nav className="nav">
        <img tabIndex="1" src={require('./img/logo.PNG')} alt="sachi logo link to home page" className="logo"></img>
            <ul>
                <li tabIndex="6">
                    <Link  to="/contact"> קשר </Link> 
                </li>
                <li tabIndex="5">
                    <Link  to="/transparency">  שקיפות  </Link> 
                </li>
                <li tabIndex="4">
                    <Link  to="/people">   אנשים </Link> 
                </li>
                <li tabIndex="3">
                    <Link  to="/how-can-you-help">  כיצד תוכלו לעזור </Link> 
                </li>
                <li tabIndex="2">
                    <Link  to="/"> סיירת חסר ייחודית </Link> 
                </li>
            </ul>
            <Social/>
            <div tabIndex="13" alt="אתר באנגלית" className="trans">ENG</div>
        </nav>
        </header>
        <main>
            <Route exact path="/" component={Home} />
            <Route  path="/how-can-you-help" component={HowCanYouHelp} />
            <Route  path="/people" component={People} />
            <Route  path="/transparency" component={Transparency} />
            <Route  path="/contact" component={Contact} />
        </main>
        <footer className="footer">
        <nav className="nav">
        <ul>
            <li tabIndex="6">
                <Link  to="/contact"> קשר </Link> 
            </li>
            <li tabIndex="5">
                <Link  to="/transparency">  שקיפות  </Link> 
            </li>
            <li tabIndex="4">
                <Link  to="/people">   אנשים </Link> 
            </li>
            <li tabIndex="3">
                <Link  to="/how-can-you-help">  כיצד תוכלו לעזור </Link> 
            </li>
            <li tabIndex="2">
                <Link  to="/"> סיירת חסר ייחודית </Link> 
            </li>
        </ul>
        <Social/>
        </nav>
        </footer>
        </div>
      </Router>
      </div>
    );
  }
}

export default App;
